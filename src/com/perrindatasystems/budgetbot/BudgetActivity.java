package com.perrindatasystems.budgetbot;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;

public abstract class BudgetActivity extends Activity {

	public void startActivityByClass(Class<? extends Activity> clz, int flags) {
		Intent intent = new Intent();
		intent.setClass(this, clz);
		intent.setFlags(flags);
		this.startActivity(intent);
	}

	public void startActivityByClass(Class<? extends Activity> clz) {
		Intent intent = new Intent();
		intent.setClass(this, clz);
		this.startActivity(intent);
	}

	public void doAlert(String message) {
		new AlertDialog.Builder(this).setMessage(message).create().show();
	}
}
