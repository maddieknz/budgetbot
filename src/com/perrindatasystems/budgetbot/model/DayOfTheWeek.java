package com.perrindatasystems.budgetbot.model;

public enum DayOfTheWeek {
	SUNDAY("Sunday", 1), MONDAY("Monday", 2), TUESDAY("Tuesday", 3), WEDNESDAY(
			"Wednesday", 4), THURSDAY("Thursday", 5), FRIDAY("Friday", 6), SATURDAY(
			"Saturday", 7);

	public final String name;
	public final int dow;

	private DayOfTheWeek(String name, int dow) {
		this.name = name;
		this.dow = dow;
	}

}
