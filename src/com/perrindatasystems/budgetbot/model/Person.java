package com.perrindatasystems.budgetbot.model;

import android.content.ContentValues;
import android.database.Cursor;

public class Person implements DatabaseObject {

	private int id = 0;
	private String name;
	private boolean active;
	private Database database;

	public Person(Database database) {
		this.database = database;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getId() {
		return this.id;
	}

	public void initFrom(Cursor cursor, String[] columnNames) {
		for (int idx = 0; idx < columnNames.length; idx++) {
			String column = columnNames[idx];
			if (column.equals("p_id"))
				this.id = cursor.getInt(idx);
			else if (column.equals("p_name"))
				this.name = cursor.getString(idx);
			else if (column.equals("p_active"))
				this.active = cursor.getInt(idx) == 0 ? false : true;
		}
	}

	@Override
	public boolean save() {
		ContentValues cv = new ContentValues();
		cv.put("p_name", this.name);
		cv.put("p_active", this.active);
		if (this.id == 0)
			this.id = this.database.insert(this, cv);
		else
			this.database.update(this, cv);
		return false;
	}

	@Override
	public String getTableName() {
		return "p_person";
	}

	@Override
	public String getPrimaryKeyColumn() {
		return "p_id";
	}

}
