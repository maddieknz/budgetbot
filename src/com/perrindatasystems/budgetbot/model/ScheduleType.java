package com.perrindatasystems.budgetbot.model;

public enum ScheduleType {
	WEEKLY("Weekly"), BIWEEKLY("Bi-weekly"), BIMONTHLY("Bi-monthly"), MONTHLY(
			"Monthly"), OTHER("Other");

	public final String name;

	private ScheduleType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
