package com.perrindatasystems.budgetbot.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BudgetDatabase extends Database {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "budgetbot.db";

	public BudgetDatabase(Context context) {
		super(new Opener(context).getWritableDatabase());
	}

	public static class Opener extends SQLiteOpenHelper {

		Opener(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db
					.execSQL("CREATE TABLE i_item (i_id integer PRIMARY KEY  AUTOINCREMENT  NOT NULL , i_name VARCHAR NOT NULL , i_p_id integer, i_t_id integer NOT NULL , i_notes TEXT)");
			db
					.execSQL("CREATE TABLE l_ledger (l_id integer PRIMARY KEY  AUTOINCREMENT  NOT NULL , l_actual BOOL NOT NULL  DEFAULT 0, l_date datetime NOT NULL , l_amount real NOT NULL , l_i_id integer NOT NULL , l_s_id integer, l_notes TEXT);");
			db
					.execSQL("CREATE TABLE p_person (p_id INTEGER PRIMARY KEY  NOT NULL ,p_name VARCHAR NOT NULL ,p_active BOOL NOT NULL  DEFAULT (1) );");
			db
					.execSQL("CREATE TABLE s_segment (s_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , s_i_id integer NOT NULL , s_start DATETIME NOT NULL , s_end DATETIME NOT NULL , s_budget REAL NOT NULL , s_sched_type VARCHAR NOT NULL , s_dm integer, s_dm2 integer, s_dw integer, s_next datetime);");
			db
					.execSQL("CREATE TABLE t_type (t_id integer PRIMARY KEY  AUTOINCREMENT  NOT NULL , t_type varchar NOT NULL , t_name varchar NOT NULL );");
			db
					.execSQL("CREATE INDEX ledger_item ON l_ledger (l_i_id ASC, l_s_id ASC)");
			db.execSQL("CREATE INDEX item_type ON i_item (i_t_id ASC)");
			db.execSQL("CREATE INDEX segment_item ON s_segment (s_i_id ASC)");
			db.execSQL("INSERT INTO t_type VALUES(1,'income','Pay Check');");
			db.execSQL("INSERT INTO t_type VALUES(2,'expense','Grocery');");
			db.execSQL("INSERT INTO t_type VALUES(3,'expense','Bill');");
			db
					.execSQL("INSERT INTO t_type VALUES(4,'expense','Rent/Mortgage')");
			db.execSQL("INSERT INTO t_type VALUES(5,'expense','Car Payment');");
			db.execSQL("INSERT INTO t_type VALUES(6,'expense','Gas');");
			db.execSQL("INSERT INTO t_type VALUES(7,'expense','Bank fees');");
			db.execSQL("INSERT INTO t_type VALUES(8,'luxury','Movies');");
			db.execSQL("INSERT INTO t_type VALUES(9,'luxury','Eating Out')");
			db
					.execSQL("INSERT INTO t_type VALUES(10,'luxury','Parks/Amusement');");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}
	}
}
