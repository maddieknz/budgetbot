package com.perrindatasystems.budgetbot.model;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.NoSuchElementException;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Database {

	private SQLiteDatabase database;

	public Database(SQLiteDatabase database) {
		this.database = database;
	}

	public <T extends DatabaseObject> Iterator<T> getCursorIterator(
			final Cursor cursor, final Class<T> objClass) {
		return new Iterator<T>() {

			String[] columnNames = cursor.getColumnNames();

			@Override
			public boolean hasNext() {
				return !cursor.isAfterLast();
			}

			@Override
			public T next() {
				if (!cursor.moveToNext())
					throw new NoSuchElementException();
				T obj = Database.this.createInstance(objClass);
				obj.initFrom(cursor, this.columnNames);
				if (cursor.isAfterLast())
					cursor.close();
				return obj;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	public <T extends DatabaseObject> int getCount(Class<T> objClass) {
		T object = this.createInstance(objClass);
		Cursor cursor = this.query("SELECT COUNT(*) FROM "
				+ object.getTableName());
		int count = 0;
		if (cursor.moveToNext())
			count = cursor.getInt(0);
		cursor.close();
		return count;
	}

	public <T extends DatabaseObject> Iterator<T> getIterator(Class<T> objClass) {
		T object = this.createInstance(objClass);
		return this.getCursorIterator(this.query("SELECT * FROM "
				+ object.getTableName()), objClass);
	}

	@SuppressWarnings("unchecked")
	public <T extends DatabaseObject> T[] getArray(Class<T> objClass) {
		T object = this.createInstance(objClass);
		Cursor cursor = this.query("SELECT * FROM " + object.getTableName());
		T[] array = (T[]) Array.newInstance(objClass, cursor.getCount());
		String[] columnNames = cursor.getColumnNames();
		for (int idx = 0; idx < array.length; idx++) {
			if (!cursor.moveToNext())
				break;
			if (idx > 0)
				object = this.createInstance(objClass);
			object.initFrom(cursor, columnNames);
			array[idx] = object;
		}
		cursor.close();
		return array;
	}

	public Cursor query(String sql, Object... args) {
		String[] stringArgs = new String[args.length];
		for (int i = 0; i < args.length; i++)
			stringArgs[i] = args[i].toString();
		return this.database.rawQuery(sql, stringArgs);
	}

	protected <T> T createInstance(Class<T> objClass) {
		try {
			return objClass.getConstructor(Database.class).newInstance(this);
		} catch (Exception e) {
			throw new RuntimeException("Failed to create intance of "
					+ objClass, e);
		}
	}

	public void close() {
		this.database.close();
	}

	public <T extends DatabaseObject> int insert(T obj, ContentValues cv) {
		return (int) this.database.insert(obj.getPrimaryKeyColumn(), "", cv);
	}

	public <T extends DatabaseObject> void update(T obj, ContentValues cv) {
		this.database.update(obj.getPrimaryKeyColumn(), cv, obj
				.getPrimaryKeyColumn()
				+ "=" + obj.getId(), null);
	}
}
