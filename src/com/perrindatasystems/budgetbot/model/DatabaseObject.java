package com.perrindatasystems.budgetbot.model;

import android.database.Cursor;

public interface DatabaseObject {

	public String getPrimaryKeyColumn();

	public int getId();

	public String getTableName();

	public void initFrom(Cursor cursor, String[] columnNames);

	public boolean save();
}
