package com.perrindatasystems.budgetbot.welcome;

import java.util.ArrayList;
import java.util.Arrays;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.perrindatasystems.budgetbot.BudgetActivity;
import com.perrindatasystems.budgetbot.R;
import com.perrindatasystems.budgetbot.helpers.Dialogs;
import com.perrindatasystems.budgetbot.helpers.Numbers;
import com.perrindatasystems.budgetbot.model.DayOfTheWeek;
import com.perrindatasystems.budgetbot.model.ScheduleType;
import com.perrindatasystems.budgetbot.ui.StandardWizardPage;
import com.perrindatasystems.budgetbot.ui.Wizard;
import com.perrindatasystems.budgetbot.ui.YesNoWizardPage;

public class WelcomeWizardActivity extends BudgetActivity {

	private WelcomeWizard wizard;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.wizard);
		this.wizard = new WelcomeWizard();
	}

	private class WelcomeWizard extends Wizard {

		ArrayList<String> people = new ArrayList<String>();
		private YesNoWizardPage addIncomePage;
		private String incomeName = "My pay check";
		private String incomeBearer = null;
		private ScheduleType incomeSchedule = ScheduleType.BIWEEKLY;
		private double incomeBudget = 1000;
		private boolean incomeVariable = true;

		public WelcomeWizard() {
			super(WelcomeWizardActivity.this);
			this.addWizardPage(new CreatePeople());
			this.addWizardPage(this.addIncomePage = new YesNoWizardPage(
					R.layout.wizard_welcome_ask_add_income, true));
			this.addWizardPage(new IncomePage());
			this.addWizardPage(new IncomeSchedulePage());
			this.addWizardPage(new StandardWizardPage(
					R.layout.wizard_welcome_done));
		}

		@Override
		public void onFinish() {
			/*
			 * BudgetDatabase db = new
			 * BudgetDatabase(WelcomeWizardActivity.this); for (String name :
			 * this.people) { Person person = new Person(db);
			 * person.setName(name); person.setActive(true); person.save(); }
			 * db.close();
			 */
			super.onFinish();
		}

		private class CreatePeople extends StandardWizardPage {
			public CreatePeople() {
				super(R.layout.wizard_welcome_create_people);
			}

			@Override
			public boolean onNextButton() {
				EditText editText = (EditText) this
						.findViewById(R.id.welcome_create_people_text);
				String[] names = editText.getText().toString().split("\n");
				WelcomeWizard.this.people.clear();

				for (String name : names) {
					if (name.length() == 0)
						continue;
					if (WelcomeWizard.this.people.contains(name)) {
						Dialogs.messageDialog(this.activity,
								"You entered the same name twice.", null);
						return false;
					}
					WelcomeWizard.this.people.add(name);
				}

				if (WelcomeWizard.this.people.isEmpty()) {
					Dialogs.messageDialog(this.activity,
							"You must enter at least one name.", null);
					return false;
				}
				return true;
			}
		}

		private class IncomePage extends StandardWizardPage {

			public IncomePage() {
				super(R.layout.wizard_welcome_income);
			}

			@Override
			public void onShow() {
				Spinner spinner = (Spinner) this
						.findViewById(R.id.wizard_welcome_income_person);
				spinner.setAdapter(new ArrayAdapter<String>(
						WelcomeWizardActivity.this, R.layout.spinner_text_item,
						WelcomeWizard.this.people
								.toArray(new String[WelcomeWizard.this.people
										.size()])));
				spinner.setSelection(0);
				spinner = (Spinner) this
						.findViewById(R.id.wizard_welcome_income_schedule);
				spinner.setAdapter(new ArrayAdapter<ScheduleType>(
						WelcomeWizardActivity.this, R.layout.spinner_text_item,
						ScheduleType.values()));
				spinner.setSelection(0);
				super.onShow();
			}

			@Override
			public void recordState() {
				TextView textView = (TextView) this
						.findViewById(R.id.wizard_welcome_income_name);
				WelcomeWizard.this.incomeName = textView.getText().toString();
				Spinner spinner = (Spinner) this
						.findViewById(R.id.wizard_welcome_income_person);
				WelcomeWizard.this.incomeBearer = (String) spinner
						.getSelectedItem();
				spinner = (Spinner) this
						.findViewById(R.id.wizard_welcome_income_schedule);
				WelcomeWizard.this.incomeSchedule = (ScheduleType) spinner
						.getSelectedItem();
				textView = (TextView) this
						.findViewById(R.id.wizard_welcome_income_amount);
				WelcomeWizard.this.incomeBudget = Numbers.parseDouble(textView
						.getText().toString());
				CheckBox checkbox = (CheckBox) this
						.findViewById(R.id.wizard_welcome_income_variable);
				WelcomeWizard.this.incomeVariable = checkbox.isChecked();
			}

			@Override
			public boolean onNextButton() {

				if (WelcomeWizard.this.incomeName.length() == 0) {
					Dialogs.messageDialog(this.activity,
							"Name for this income is required.", null);
					return false;
				}
				if (WelcomeWizard.this.people
						.contains(WelcomeWizard.this.incomeBearer) == false) {
					Dialogs.messageDialog(this.activity,
							"Must select owner for this income.", null);
					return false;
				}

				if (WelcomeWizard.this.incomeBudget <= 0) {
					Dialogs.messageDialog(this.activity,
							"Valid take home pay must be entered.", null);
					return false;

				}

				return true;
			}

			@Override
			public boolean willShow() {
				if (!WelcomeWizard.this.addIncomePage.isYes())
					return false;
				return super.willShow();
			}

		}

		private class IncomeSchedulePage extends StandardWizardPage {

			public IncomeSchedulePage() {
				super(R.layout.wizard_welcome_income_schedule);
			}

			@Override
			public void onShow() {
				this.adjustVisibility(WelcomeWizard.this.incomeSchedule, this
						.getView(WelcomeWizardActivity.this));
				Spinner spinner = (Spinner) this
						.findViewById(R.id.wizard_welcome_income_dow);
				spinner.setAdapter(new ArrayAdapter<DayOfTheWeek>(
						WelcomeWizardActivity.this, R.layout.spinner_text_item,
						DayOfTheWeek.values()));
			}

			private void adjustVisibility(ScheduleType type, View view) {
				if (view.getTag() != null
						&& view.getTag().toString().length() > 0
						&& Arrays.asList(view.getTag().toString().split(","))
								.contains(type.name()) == false) {
					view.setVisibility(View.GONE);
					return;
				}
				view.setVisibility(View.VISIBLE);
				if (view instanceof ViewGroup) {
					ViewGroup group = (ViewGroup) view;
					for (int idx = 0; idx < group.getChildCount(); idx++)
						this.adjustVisibility(type, group.getChildAt(idx));
				}
			}

			@Override
			public boolean willShow() {
				if (!WelcomeWizard.this.addIncomePage.isYes())
					return false;
				if (WelcomeWizard.this.incomeSchedule == ScheduleType.OTHER)
					return false;
				return super.willShow();
			}

		}
	}
}
