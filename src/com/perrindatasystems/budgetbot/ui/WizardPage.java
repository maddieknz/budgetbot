package com.perrindatasystems.budgetbot.ui;

import android.app.Activity;
import android.content.Context;
import android.view.View;

public abstract class WizardPage {

	protected Activity activity;

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public View findViewById(int id) {
		return this.activity.findViewById(id);
	}

	public abstract View getView(Context context);

	public boolean onNextButton() {
		return true;
	}

	public boolean willShow() {
		return true;
	}

	public void onShow() {

	}

	public void recordState() {

	}
}
