package com.perrindatasystems.budgetbot.ui;

import java.util.Vector;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;

import com.perrindatasystems.budgetbot.R;

public class Wizard implements OnClickListener {

	protected Activity activity;
	private Vector<WizardPage> pages = new Vector<WizardPage>();
	private int pageIndex = 0;
	private WizardPage currentPage = null;
	private FrameLayout frame = null;
	private Button previousButton = null;
	private Button nextButton;
	private Button finishButton;

	public Wizard(Activity activity) {
		this.activity = activity;
		this.frame = (FrameLayout) activity.findViewById(R.id.wizard_frame);

		this.previousButton = (Button) activity
				.findViewById(R.id.wizard_previous);
		this.previousButton.setOnClickListener(this);

		this.nextButton = (Button) activity.findViewById(R.id.wizard_next);
		this.nextButton.setOnClickListener(this);

		this.finishButton = (Button) activity.findViewById(R.id.wizard_finish);
		this.finishButton.setOnClickListener(this);
	}

	public void addWizardPage(WizardPage page) {
		this.pages.add(page);
		page.setActivity(this.activity);
		if (this.currentPage == null)
			this.setPage(page);
		else
			this.updateButtons();
	}

	private void updateButtons() {
		int index = this.pages.indexOf(this.currentPage);
		if (index == 0)
			this.previousButton.setVisibility(View.INVISIBLE);
		else
			this.previousButton.setVisibility(View.VISIBLE);

		if (index == this.pages.size() - 1) {
			this.finishButton.setVisibility(View.VISIBLE);
			this.nextButton.setVisibility(View.INVISIBLE);
		} else {
			this.finishButton.setVisibility(View.VISIBLE);
			this.nextButton.setVisibility(View.VISIBLE);
		}
	}

	private void setPage(WizardPage page) {
		this.currentPage = page;
		this.frame.removeAllViews();
		this.frame.addView(page.getView(this.activity));
		this.updateButtons();
		page.onShow();
	}

	@Override
	public void onClick(View v) {
		if (this.currentPage != null)
			this.currentPage.recordState();

		if (v == this.nextButton && this.currentPage.onNextButton())
			this.pageNext();
		else if (v == this.finishButton)
			this.finish();
		else if (v == this.previousButton)
			this.pageLast();
	}

	private void finish() {
		while (this.pageIndex < this.pages.size()) {
			if (!this.pages.get(this.pageIndex).onNextButton())
				return;
			this.pageIndex++;
		}
		this.onFinish();
	}

	private void pageLast() {
		for (;;) {
			if (this.pageIndex == 0)
				break;
			this.pageIndex--;
			WizardPage page = this.pages.get(this.pageIndex);
			if (page.willShow()) {
				this.setPage(page);
				return;
			}
		}
	}

	private void pageNext() {
		for (;;) {
			this.pageIndex++;
			if (this.pageIndex >= this.pages.size()) {
				this.onFinish();
				return;
			}
			WizardPage page = this.pages.get(this.pageIndex);
			if (page.willShow()) {
				this.setPage(page);
				return;
			}
		}
	}

	public void onFinish() {
		this.activity.finish();
	}
}
