package com.perrindatasystems.budgetbot.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public class StandardWizardPage extends WizardPage {

	private int layoutId;

	public StandardWizardPage(int layoutId) {
		this.layoutId = layoutId;
	}

	private View view = null;

	@Override
	public View getView(Context context) {
		if (this.view == null)
			this.view = LayoutInflater.from(context).inflate(this.layoutId,
					null);
		return this.view;
	}
}
