package com.perrindatasystems.budgetbot.ui;

import android.widget.RadioButton;

import com.perrindatasystems.budgetbot.R;

public class YesNoWizardPage extends StandardWizardPage {

	private boolean value;

	public YesNoWizardPage(int layoutId, boolean defaultValue) {
		super(layoutId);
		this.value = defaultValue;
	}

	@Override
	public void onShow() {
		if (this.value)
			((RadioButton) this.findViewById(R.id.wizard_yes)).setChecked(true);
		else
			((RadioButton) this.findViewById(R.id.wizard_no)).setChecked(true);
	}

	@Override
	public boolean onNextButton() {
		RadioButton wizardYes = (RadioButton) this
				.findViewById(R.id.wizard_yes);
		this.value = wizardYes.isChecked();
		return true;
	}

	public boolean isYes() {
		return this.value;
	}

}
