package com.perrindatasystems.budgetbot.helpers;

public interface RunnableWithParameter<Type> {

    public void run(Type parameter);
}
