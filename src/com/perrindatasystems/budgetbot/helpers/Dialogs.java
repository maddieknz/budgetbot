package com.perrindatasystems.budgetbot.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.widget.EditText;

public class Dialogs {

	public static void confirmDialog(Context context, String message,
			final Runnable runnable) {
		OnClickListener listener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (which == DialogInterface.BUTTON1)
					runnable.run();
			}
		};

		AlertDialog alert = new AlertDialog.Builder(context)
				.setMessage(message).setCancelable(true).setPositiveButton(
						"OK", listener).setNegativeButton("Cancel", listener)
				.create();

		alert.show();
	}

	public static void inputDialog(Context context, String message,
			String initialText, final RunnableWithParameter<String> runnable) {

		final EditText view = new EditText(context);
		view.setText(initialText);

		OnClickListener listener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (which == DialogInterface.BUTTON1)
					runnable.run(view.getText().toString());
			}
		};

		AlertDialog alert = new AlertDialog.Builder(context).setView(view)
				.setMessage(message).setCancelable(true).setPositiveButton(
						"OK", listener).setNegativeButton("Cancel", listener)
				.create();

		alert.show();
	}

	public static void messageDialog(Context context, String message,
			final Runnable runnable) {
		OnClickListener listener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (runnable != null)
					runnable.run();
			}
		};

		AlertDialog alert = new AlertDialog.Builder(context).setPositiveButton(
				"OK", listener).setMessage(message).create();
		alert.show();
	}

	public static void messageDialogWithView(Context context,
			final Runnable runnable, View view) {
		OnClickListener listener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (runnable != null)
					runnable.run();
			}
		};

		AlertDialog alert = new AlertDialog.Builder(context).setPositiveButton(
				"OK", listener).setView(view).create();
		alert.show();
	}

	private static final Integer[] buttonOrder = { Dialog.BUTTON_POSITIVE,
			Dialog.BUTTON_NEGATIVE, Dialog.BUTTON_NEUTRAL };

	public static void dialogWithCustomButtons(Context context, String message,
			final String[] buttons, final RunnableWithParameter<String> runnable) {

		OnClickListener listener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				int idx = Arrays2.indexOf(buttonOrder, which);
				if (runnable != null)
					runnable.run(buttons[idx]);
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message);

		if (buttons.length > 0)
			builder.setPositiveButton(buttons[0], listener);
		if (buttons.length > 1)
			builder.setNegativeButton(buttons[1], listener);
		if (buttons.length > 2)
			builder.setNeutralButton(buttons[2], listener);

		builder.setCancelable(false);
		builder.create().show();

	}
}
