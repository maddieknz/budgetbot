package com.perrindatasystems.budgetbot.helpers;

import android.app.Dialog;

public interface DialogResultHandler<T> {

    public void handleResult(Dialog dialog, String button, T result);

}
