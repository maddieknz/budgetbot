package com.perrindatasystems.budgetbot.helpers;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.List;

public class Arrays2 {

    @SuppressWarnings("unchecked")
    public static <T> T[] newArray(Class<T> elementClass, int size) {
	return (T[]) Array.newInstance(elementClass, size);
    }

    public static <T> int search(T[] array, T object) {

	for (int i = 0; i < array.length; i++)
	    if (array[i] == object)
		return i;
	return -1;
    }

    public static <T> int indexOf(T[] array, T object) {
	for (int i = 0; i < array.length; i++)
	    if ((array[i] == null && object == null) || array[i].equals(object))
		return i;
	return -1;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] join(T[]... arrays) {

	int size = 0, offset = 0, i;
	for (i = 0; i < arrays.length; i++)
	    size += arrays[i].length;
	T[] result = (T[]) Array.newInstance(arrays[0].getClass()
		.getComponentType(), size);
	for (i = 0; i < arrays.length; i++) {
	    System.arraycopy(arrays[i], 0, result, offset, arrays[i].length);
	    offset += arrays[i].length;
	}
	return result;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] subArray(T[] array, int start) {
	int length = array.length - start;
	T[] out = (T[]) Array.newInstance(array.getClass().getComponentType(),
		length);
	System.arraycopy(array, start, out, 0, length);
	return out;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] subArray(T[] array, int start, int length) {
	T[] out = (T[]) Array.newInstance(array.getClass().getComponentType(),
		length);
	System.arraycopy(array, start, out, 0, length);
	return out;
    }

    public static int[] subArray(int[] array, int start, int length) {
	if (length == 0)
	    return new int[0];
	int[] out = new int[length];
	System.arraycopy(array, start, out, 0, length);
	return out;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] toArray(Collection<? extends T> collection,
	    Class<T> clz) {
	T[] array = (T[]) Array.newInstance(clz, collection.size());
	return collection.toArray(array);
    }

    public static <T> List<T> asList(T... elements) {
	return java.util.Arrays.asList(elements);
    }

    public static <T> boolean contains(T[] array, Object element) {
	if (element == null) {
	    for (T item : array)
		if (item == null)
		    return true;
	} else {
	    for (T item : array)
		if (element.equals(item))
		    return true;
	}
	return false;
    }

    public static String[] toStringArray(Collection<?> collection) {
	String[] strings = new String[collection.size()];
	int i = 0;
	for (Object obj : collection)
	    strings[i++] = obj.toString();
	return strings;
    }

    public static List<String> toStringList(Collection<?> collection) {
	return asList(toStringArray(collection));
    }

}
