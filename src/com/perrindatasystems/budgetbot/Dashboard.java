package com.perrindatasystems.budgetbot;

import android.os.Bundle;

import com.perrindatasystems.budgetbot.model.BudgetDatabase;
import com.perrindatasystems.budgetbot.model.Person;
import com.perrindatasystems.budgetbot.welcome.WelcomeWizardActivity;

public class Dashboard extends BudgetActivity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.main);

		BudgetDatabase db = new BudgetDatabase(this);
		if (db.getCount(Person.class) == 0)
			this.startActivityByClass(WelcomeWizardActivity.class);
		else {

		}

		db.close();
	}
}